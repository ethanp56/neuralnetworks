import theano as th
from theano import tensor as T
import keras
from keras.datasets import mnist


# Prepare data
(x_train, y_train), (x_test, y_test) = mnist.load_data()
print 'x_train orig shape: ', x_train.shape
print 'y_train orig shape: ' y_train.shape
print 'x_test orig shape: ', x_test.shape
print 'y_test orig shape: ' y_test.shape
x_train.reshape((-1, x_train.shape[1] * x_train.shape[2]))
print 'x_train reshaped: ' x_train.shape
x_test.reshape((-1, x_test.shape[1], x_test.shape[2]))
print 'x_test reshaped:' x_test.shape
# TODO: Scale input

# Initialize Weights
init = keras.initializers.he_normal()

hidden1 = 50
W1 = init((x_train.shape[1], hidden1))
b1 = init((hidden1))
print 'W1: ', W1.shape
print 'b1: ', b1.shape


# Forward Propagation
X = T.fmatrix()
y = T.fvector()

y_hat = T.nn.softmax(T.dot(X, W1) + b1)
# loss = cross-entropy

# Back Propagation
# T.grad(loss, [all params])

# Gradient Descent
# updates

import numpy as np
import theano
from theano import tensor as T
import matplotlib.pyplot as plt

from load import mnist
from nn_utils import *

verbosity = 0
use_test_value = False
batch_size = 128
num_plot = 9

def D(X, p, p_drop_input, p_drop_hidden):
    X = dropout(X, p_drop_input)

    h1 = T.nnet.relu(T.dot(X, p['Dw1']) + p['Db1'])
    h1 = dropout(h1, p_drop_hidden)

    h2 = T.nnet.relu(T.dot(h1, p['Dw2']) + p['Db2'])
    h2 = dropout(h2, p_drop_hidden)

    py_x = T.nnet.sigmoid(T.dot(h2, p['Dw3']) + p['Db3'])
    return py_x

def G(p, num_samples):
    z = floatX(np.random.normal(0.0, 1.0, (num_samples, p['Gw1'].shape[0].eval())))

    h1 = T.nnet.relu(T.dot(z, p['Gw1']) + p['Gb1'])

    h2 = T.nnet.relu(T.dot(h1, p['Gw2']) + p['Gb2'])

    x = T.nnet.sigmoid(T.dot(h2, p['Gw3']) + p['Gb3'])
    return x

def evaluate_model():
    # Evaluate discriminator accuracy on fake data
    if verbosity >= 1:
        print 'Generating fake test images...'
    teXfake = G(Gp, num_test).eval()
    pred_fake = predict(teXfake)
    acc_fake = np.mean(np.round(teYfake) == pred_fake)
    if verbosity >= 0:
        print 'Test Acc Fake:', acc_fake
        print predict_prob(teXfake)
    if (num_plot > 0):
        for i in range(num_plot):
            image_fake = 1. - teXfake[i, :].reshape((28, 28))
            plt.subplot(num_plot * 100 + 11 + i)
            plt.imshow(image_fake, cmap='Greys')
        plt.show()

    # Evaluate discriminator accuracy on real data
    pred_real = predict(teXreal)
    acc_real = np.mean(np.round(teYreal) == pred_real)
    if verbosity >= 0:
        print 'Test Acc Real:', acc_real
        print predict_prob(teXreal)
    return acc_fake, acc_real

def init_params():
    # Discriminator Parameters
    print 'Initializing parameters...'
    Dw1 = init_weights((784, 400))
    Db1 = init_biases((400,))
    Dw2 = init_weights((400, 200))
    Db2 = init_biases((200,))
    Dw3 = init_weights((200, 1))
    Db3 = init_biases((1,))
    Dp = {'Dw1': Dw1, 'Db1': Db1, 'Dw2': Dw2, 'Db2': Db2, 'Dw3': Dw3, 'Db3': Db3}

    # Generator Parameters
    Gw1 = init_weights((50, 625))
    Gb1 = init_biases((625))
    Gw2 = init_weights((625, 625))
    Gb2 = init_biases((625))
    Gw3 = init_weights((625, 784))
    Gb3 = init_biases((784))
    Gp = {'Gw1': Gw1, 'Gb1': Gb1, 'Gw2': Gw2, 'Gb2': Gb2, 'Gw3': Gw3, 'Gb3': Gb3}
    return Dp, Gp

# Data Setup
print 'Setting up data...'
trXreal, teXreal, trYclass, teYclass = mnist(onehot=True)
print trXreal[0,:]
real_value = .9
fake_value = 0.
trYreal = init_labels((trYclass.shape[0], 1), real_value)
trYfake = init_labels((trYclass.shape[0], 1), fake_value)
teYreal = init_labels((teYclass.shape[0], 1), real_value)
teYfake = init_labels((teYclass.shape[0], 1), fake_value)
num_train = trXreal.shape[0]
num_test = teXreal.shape[0]

X = T.fmatrix('X')
Y = T.fmatrix('Y')
if use_test_value:
    theano.config.compute_test_value = 'warn'
    X_concrete = floatX(np.zeros((4, 784)))
    Y_concrete = init_labels((4, 1), fake_value)
    X.tag.test_value = X_concrete
    Y.tag.test_value = Y_concrete

Dp, Gp = init_params()

# Setup Computation Functions
print 'Compiling computation graph...'
noise_py_x = D(X, Dp, 0.2, 0.5)
py_x = D(X, Dp, 0., 0.)
y_x = T.round(py_x)
noise_cost = T.mean(T.nnet.binary_crossentropy(noise_py_x, Y))  # D minimizes dropout cost
Dupdates = RMSprop(noise_cost, Dp.values(), lr=0.00001)

py_x_gen = D(G(Gp, num_train), Dp, 0., 0.)
Gcost = T.mean(T.nnet.binary_crossentropy(py_x_gen, trYfake))  # G maximizes dropout-free cost
Gupdates = RMSprop(-Gcost, Gp.values(), lr=0.01)

Dtrain = theano.function(inputs=[X, Y], outputs=noise_cost, updates=Dupdates, allow_input_downcast=True)
Gtrain = theano.function(inputs=[], outputs=Gcost, updates=Gupdates, allow_input_downcast=True)
predict_prob = theano.function(inputs=[X], outputs=py_x, allow_input_downcast=True)
predict = theano.function(inputs=[X], outputs=y_x, allow_input_downcast=True)

# Train!
print 'Training...'
for i in range(100):
    for i in range(1):
        if verbosity >= 1:
            print 'Generating fake train images...'
        trXfake = G(Gp, num_train).eval()
        for start, end in zip(range(0, num_train, batch_size), range(batch_size, num_train, batch_size)):
            noise_cost = Dtrain(trXfake[start:end], trYfake[start:end])
            noise_cost = Dtrain(trXreal[start:end], trYreal[start:end])
            if verbosity >= 1:
                print start, end
                print '        F:', noise_cost
                print '                              R:', noise_cost

    evaluate_model()

    if verbosity >= 1:
        print 'Generator Update...'
    Gcost = Gtrain()
    print '                    GCost:', Gcost
    threshold = .7
    while Gcost < threshold:
        Gcost = Gtrain()
        print '                    GCost:', Gcost

    evaluate_model()

__author__ = 'ethan_perez'
import matplotlib.pyplot as plt
import numpy as np
from sklearn import datasets


def generate_data(form='gaussian'):
    """
    generate data
    :return: X: input data, y: given labels
    """
    np.random.seed(0)
    if form == 'moon':
        X, y = datasets.make_moons(200, noise=0.20)
    elif form == 'circles':
        X, y = datasets.make_circles(random_state=0)
    elif form == 'biclusters':
        _, X, y = datasets.biclusters(shape=(32, 32), n_clusters=4)
    else:
        X1, y1 = datasets.make_gaussian_quantiles(cov=2.,
                                         n_samples=200, n_features=2,
                                         n_classes=2, random_state=1)
        X2, y2 = datasets.make_gaussian_quantiles(mean=(3, 3), cov=1.5,
                                         n_samples=300, n_features=2,
                                         n_classes=2, random_state=1)
        X = np.concatenate((X1, X2))
        y = np.concatenate((y1, - y2 + 1))
    return X, y


def plot_decision_boundary(pred_func, X, y):
    """
    plot the decision boundary
    :param pred_func: function used to predict the label
    :param X: input data
    :param y: given labels
    :return:
    """
    # Set min and max values and give it some padding
    x_min, x_max = X[:, 0].min() - .5, X[:, 0].max() + .5
    y_min, y_max = X[:, 1].min() - .5, X[:, 1].max() + .5
    h = 0.01
    # Generate a grid of points with distance h between them
    xx, yy = np.meshgrid(
        np.arange(
            x_min, x_max, h), np.arange(
            y_min, y_max, h))
    # Predict the function value for the whole gid
    Z = pred_func(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)
    # Plot the contour and training examples
    plt.contourf(xx, yy, Z, cmap=plt.cm.Spectral)
    plt.scatter(X[:, 0], X[:, 1], c=y, cmap=plt.cm.Spectral)
    plt.show()


class NeuralNetwork(object):
    """
    This class builds and trains a neural network
    """

    def __init__(self, layer_dims, act_fun_type='tanh',
                 reg_lambda=0.01, seed=0, num_layers=None):
        """
        :param nn_input_dim: input dimension
        :param nn_hidden_dim: the number of hidden units in each hidden layer
        :param nn_output_dim: output dimension
        :param act_fun_type: type of activation function. 3 options: 'tanh', 'sigmoid', 'relu'
        :param reg_lambda: regularization coefficient
        :param seed: random seed
        """
        self.layer_dims = layer_dims
        self.num_layers = len(layer_dims)  # Includes input and output layers
        self.act_fun_type = act_fun_type  # Type of non-linearity
        self.reg_lambda = reg_lambda  # Regularization parameter
        self.W = {}  # Weights
        self.b = {}  # Biases
        self.a = {}  # Activations
        self.z = {}  # Pre-activations
        self.dW = {}  # dL/dW
        self.db = {}  # dL/db
        self.probs = None

        # initialize the weights and biases in the network
        np.random.seed(seed)

        for hidden_layer in range(1, len(self.layer_dims)):
            self.W[hidden_layer] = np.random.randn(
                self.layer_dims[
                    hidden_layer - 1],
                self.layer_dims[hidden_layer]) / np.sqrt(
                self.layer_dims[
                    hidden_layer - 1])
            self.b[hidden_layer] = np.zeros((1, self.layer_dims[hidden_layer]))

        # Initialize output to last layer
        self.W[self.num_layers - 1] = np.random.randn(
            self.layer_dims[-2], self.layer_dims[-1]) / np.sqrt(self.layer_dims[-2])
        self.b[self.num_layers - 1] = np.zeros((1, self.layer_dims[-1]))

    def act_fun(self, z, type):
        """
        act_fun computes the activation functions
        :param z: net input
        :param type: tanh, sigmoid, or relu
        :return: activations
        """
        if type == 'tanh':
            return np.tanh(z)
        elif type == 'sigmoid':
            return 1.0 / (1.0 + np.e ** -z)
        elif type == 'relu':
            return np.vectorize(lambda x: x if x > 0.0 else 0.0)(z)

    def diff_act_fun(self, z, type):
        """
        diff_act_fun computes the derivatives of the activation functions wrt the net input
        :param z: net input
        :param type: Tanh, Sigmoid, or ReLU
        :return: the derivatives of the activation functions wrt the net input
        """
        if type == 'tanh':
            return 1 - (np.tanh(z) ** 2)
        elif type == 'sigmoid':
            e_neg_z = np.e ** -z
            return e_neg_z / ((1.0 + e_neg_z) ** 2)
        elif type == 'relu':
            return np.vectorize(lambda x: 1.0 if x > 0 else 0.0)(z)

    def feedforward(self, X, act_fun):
        """
        feedforward builds a 3-layer neural network and computes the two probabilities,
        one for class 0 and one for class 1
        :param X: input data
        :param act_fun: activation function
        :return:
        """
        self.a[0] = X  # Data is like 0th layer activation
        for layer in range(1, self.num_layers - 1):
            self.z[layer] = np.dot(
                self.a[
                    layer - 1],
                self.W[layer]) + self.b[layer]
            self.a[layer] = act_fun(self.z[layer])

        self.z[self.num_layers - 1] = np.dot(self.a[self.num_layers - 2], self.W[
                                             self.num_layers - 1]) + self.b[self.num_layers - 1]
        exp_scores = np.exp(self.z[self.num_layers - 1])
        self.probs = exp_scores / np.sum(exp_scores, axis=1, keepdims=True)
        return

    def calculate_loss(self, X, y):
        """
        calculate_loss computes the loss for prediction
        :param X: input data
        :param y: given labels
        :return: the loss for prediction
        """
        num_examples = len(X)
        self.feedforward(X, lambda x: self.act_fun(x, type=self.act_fun_type))
        prob_error = self.probs
        prob_error[range(num_examples), y] -= 1
        prob_error = np.abs(prob_error)[:, 0]
        data_loss = np.sum(np.log(prob_error)) / float(y.shape[0])

        # Add regularization term to loss
        sum_w_squared = 0.0
        for layer in self.W:
            sum_w_squared += np.sum(np.square(self.W[layer]))
        data_loss += self.reg_lambda / 2 * sum_w_squared
        return (1. / num_examples) * data_loss

    def predict(self, X):
        """
        predict infers the label of a given data point X
        :param X: input data
        :return: label inferred
        """
        self.feedforward(X, lambda x: self.act_fun(x, type=self.act_fun_type))
        return np.argmax(self.probs, axis=1)

    def backprop(self, X, y):
        """
        backprop implements backpropagation to compute the gradients used to update the parameters in the backward step
        :param X: input data
        :param y: given labels
        :return: dL/dW1, dL/b1, dL/dW2, dL/db2
        """
        num_examples = len(X)
        delta = self.probs
        delta[range(num_examples), y] -= 1

        for layer in range(self.num_layers - 1, 1, -1):
            self.dW[layer] = np.dot(self.a[layer - 1].T, delta)
            self.db[layer] = np.sum(delta, axis=0)
            delta = np.dot(delta,
                           self.W[layer].T) * self.diff_act_fun(self.z[layer - 1],
                                                                self.act_fun_type)

        self.dW[1] = np.dot(X.T, delta)
        self.db[1] = np.sum(delta, axis=0)

        # Scale down to match cost function
        for layer in self.dW:
            self.dW[layer] /= 1. * num_examples
            self.db[layer] /= 1. * num_examples

        return

    def fit_model(self, X, y, epsilon=0.01, num_passes=20000, print_loss=True):
        """
        fit_model uses backpropagation to train the network
        :param X: input data
        :param y: given labels
        :param num_passes: the number of times that the algorithm runs through the whole dataset
        :param print_loss: print the loss or not
        :return:
        """
        # Gradient descent.
        for i in range(0, num_passes):
            # Forward and Back Prop
            self.feedforward(
                X, lambda x: self.act_fun(
                    x, type=self.act_fun_type))
            self.backprop(X, y)

            # Add regularization to update and update parameters
            for layer in self.dW:
                self.dW[layer] += self.reg_lambda * self.W[layer]
                self.W[layer] += -epsilon * self.dW[layer]
                self.b[layer] += -epsilon * self.db[layer]

            # Expensive: Uses the whole dataset, so we don't want to do it too often.
            if print_loss and i % 1000 == 0:
                print("Loss after iteration %i: %f" %
                      (i, self.calculate_loss(X, y)))

    def visualize_decision_boundary(self, X, y):
        """
        visualize_decision_boundary plots the decision boundary created by the trained network
        :param X: input data
        :param y: given labels
        :return:
        """
        plot_decision_boundary(lambda x: self.predict(x), X, y)


def main():
    # generate and visualize Make-Moons dataset
    show_raw_data = False
    X, y = generate_data()

    if show_raw_data:
        plt.scatter(X[:, 0], X[:, 1], s=40, c=y, cmap=plt.cm.Spectral)
        plt.show()

    layers = [2, 10, 10, 10, 10, 2]
    model = NeuralNetwork(
        layer_dims=layers,
        act_fun_type='tanh',
        reg_lambda=0.003,
        num_layers=len(layers))
    model.fit_model(X, y, num_passes=30001, epsilon=0.01 * 30)
    model.visualize_decision_boundary(X, y)


if __name__ == "__main__":
    main()

__author__ = 'ethan_perez'

import os
import random
import time

import nn

# Load MNIST dataset
from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets('MNIST_data', one_hot=True)

# mnist.train._num_examples = 100

# Import Tensorflow and start a session
import tensorflow as tf
sess = tf.InteractiveSession()


def main():
    # Specify training parameters
    reconstruction = True
    optimizer_name = 'sgd'
    act_name = 'relu'
    keep_prob_val = 1.0
    batch_size = 1
    max_num_epochs = 10000

    if reconstruction:
        problem_type = 'reconstruction'
    else:
        problem_type = 'classification'

    result_dir_base = 'results/' + problem_type + '/' + optimizer_name + '/' + act_name
    result_dir_training = './' + result_dir_base + '_training/'  # training results saved in this dir
    result_dir_validation = './' + result_dir_base + '_validation/'  # val results saved in this dir
    result_dir_testing = './' + result_dir_base + '_testing/'  # test results saved in this dir

    print result_dir_base
    print mnist.train._num_examples, 'train examples'
    print mnist.validation._num_examples, 'validation examples'
    print mnist.test._num_examples, 'test examples'

    batches_per_epoch = mnist.train._num_examples / float(batch_size)
    max_step = int(round(batches_per_epoch * max_num_epochs))  # Maximum number of iterations
    start_time = time.time()  # start timing

    # placeholders for input data and input labels
    x = tf.placeholder(tf.float32, [None, 784])
    y_ = tf.placeholder(tf.float32, [None, 10])  # TODO: Fix this! Causing error?

    # reshape the input image
    if reconstruction:
        x_image = x
    else:
        x_image = tf.reshape(x, [-1, 28, 28, 1])

    # Set up network
    keep_prob = tf.placeholder(tf.float32)
    if reconstruction:
        hidden_layer_sizes = [500, 200, 100, 200, 500]
        y_pred = tf_nn.init_rec_dense_with_summaries(x_image, hidden_layer_sizes, act_name=act_name)
    else:
        y_pred = tf_nn.init_cnn_with_summaries(x_image, keep_prob, act_name=act_name)

    # Set up training with visualization
    with tf.name_scope('objective'):
        if reconstruction:
            with tf.name_scope('total'):
                objective = tf.reduce_mean(tf.reduce_mean(
                    tf.pow(tf.sub(x_image, y_pred), 2), reduction_indices=[1]))
            tf.scalar_summary('mse-reconstruction-loss', objective)
        else:
            with tf.name_scope('total'):
                objective = tf.reduce_mean(-tf.reduce_sum(
                    y_ * tf.log(y_pred), reduction_indices=[1]))
            tf.scalar_summary('cross-entropy', objective)

    with tf.name_scope('train'):
        if optimizer_name == 'adam':
            train_step = tf.train.AdamOptimizer(1e-4).minimize(objective)
        elif optimizer_name == 'adadelta':
            train_step = tf.train.AdadeltaOptimizer(3e-1).minimize(objective)
        elif optimizer_name == 'sgd':
            train_step = tf.train.GradientDescentOptimizer(1e1).minimize(objective)
        elif optimizer_name == 'rmsprop':
            train_step = tf.train.RMSPropOptimizer(1e-3).minimize(objective)
        elif optimizer_name == 'momentum':
            train_step = tf.train.MomentumOptimizer(3e-2, 0.9).minimize(objective)

    if reconstruction:
        with tf.name_scope('train_error'):
            with tf.name_scope('train_reconstruction_loss'):
                train_obj = tf.reduce_mean(tf.reduce_mean(
                    tf.pow(tf.sub(x_image, y_pred), 2), reduction_indices=[1]))
            tf.scalar_summary('train_reconstruction_loss', train_obj)

        with tf.name_scope('validation_error'):
            with tf.name_scope('validation_reconstruction_loss'):
                validation_obj = tf.reduce_mean(tf.reduce_mean(
                    tf.pow(tf.sub(x_image, y_pred), 2), reduction_indices=[1]))
            tf.scalar_summary('validation_reconstruction_loss', validation_obj)

        with tf.name_scope('test_error'):
            with tf.name_scope('test_reconstruction_loss'):
                test_obj = tf.reduce_mean(tf.reduce_mean(
                    tf.pow(tf.sub(x_image, y_pred), 2), reduction_indices=[1]))
            tf.scalar_summary('test_reconstruction_loss', test_obj)
    else:
        with tf.name_scope('train_accuracy'):
            with tf.name_scope('train_correct_prediction'):
                train_correct_prediction = tf.equal(
                    tf.argmax(
                        y_pred, 1), tf.argmax(
                        y_, 1))
            with tf.name_scope('train_accuracy'):
                train_obj = tf.reduce_mean(tf.cast(train_correct_prediction, tf.float32))
            tf.scalar_summary('train_accuracy', train_obj)

        with tf.name_scope('validation_accuracy'):
            with tf.name_scope('validation_correct_prediction'):
                validation_correct_prediction = tf.equal(
                    tf.argmax(
                        y_pred, 1), tf.argmax(
                        y_, 1))
            with tf.name_scope('validation_accuracy'):
                validation_obj = tf.reduce_mean(tf.cast(validation_correct_prediction, tf.float32))
            tf.scalar_summary('validation_accuracy', validation_obj)

        with tf.name_scope('test_accuracy'):
            with tf.name_scope('test_correct_prediction'):
                test_correct_prediction = tf.equal(
                    tf.argmax(
                        y_pred, 1), tf.argmax(
                        y_, 1))
            with tf.name_scope('test_accuracy'):
                test_obj = tf.reduce_mean(tf.cast(test_correct_prediction, tf.float32))
            tf.scalar_summary('test_accuracy', test_obj)

    # Add a scalar summary for the snapshot loss.
    # Build the summary operation based on the TF collection of Summaries.
    # iterations (equivalently, after each epoch)
    summary_op = tf.merge_all_summaries()

    # Add the variable initializer Op.
    init = tf.initialize_all_variables()

    # Create a saver for writing training checkpoints.
    saver = tf.train.Saver()

    # Instantiate a SummaryWriter to output summaries and the Graph.
    train_writer = tf.train.SummaryWriter(result_dir_training + '_training', sess.graph)
    validation_writer = tf.train.SummaryWriter(result_dir_validation + '_validation', sess.graph)
    test_writer = tf.train.SummaryWriter(result_dir_testing + '_testing', sess.graph)

    # Run the Op to initialize the variables.
    sess.run(init)

    # Run training
    for i in range(max_step):
        # Make the data batch, which is used in the training iteration.
        batch = mnist.train.next_batch(batch_size)
        if i % 55000 == 0:
            # Output the training objective value every 100 iterations
            print('Epoch %d, Training Objective %g' %
                  (i / 55000, train_obj.eval(
                   feed_dict={
                       x: batch[0],
                       y_: batch[1],
                       keep_prob: 1.0})))

            # Update the events file which is used to monitor the training
            summary_str = sess.run(
                summary_op,
                feed_dict={
                    x: batch[0],
                    y_: batch[1],
                    keep_prob: keep_prob_val})
            train_writer.add_summary(summary_str, i)
            train_writer.flush()

        # Save the checkpoints every so often and track progress on val and test data
        if (i % 55000 == 0 or i == max_step) and i != 0:
            # Track progress on Validation Data
            val_obj_num = validation_obj.eval(feed_dict={
                x: mnist.validation.images, y_: mnist.validation.labels, keep_prob: 1.0})
            print 'Validation Objective ' + str(val_obj_num)
            summary_str = sess.run(
                summary_op,
                feed_dict={
                    x: mnist.validation.images,
                    y_: mnist.validation.labels,
                    keep_prob: 1.0})
            validation_writer.add_summary(summary_str, i)
            validation_writer.flush()

            # Track progress on Test Data
            test_obj_num = test_obj.eval(feed_dict={
                    x: mnist.test.images, y_: mnist.test.labels, keep_prob: 1.0})
            print 'Test Objective ' + str(test_obj_num)
            summary_str = sess.run(
                summary_op,
                feed_dict={
                    x: mnist.test.images,
                    y_: mnist.test.labels,
                    keep_prob: 1.0})
            test_writer.add_summary(summary_str, i)
            test_writer.flush()

            # Save Progress
            checkpoint_file = os.path.join(result_dir_training, 'checkpoint')
            saver.save(sess, checkpoint_file, global_step=i)

        train_step.run(
            feed_dict={
                x: batch[0],
                y_: batch[1],
                keep_prob: keep_prob_val})  # run one train_step

    print('test objective %g' % test_obj.eval(feed_dict={
        x: mnist.test.images, y_: mnist.test.labels, keep_prob: 1.0}))

    stop_time = time.time()
    print('The training takes %f second to finish' %
          (stop_time - start_time))  # NB: 300 iters/min for classification

if __name__ == '__main__':
    main()

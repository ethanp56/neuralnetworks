import tensorflow as tf
from tensorflow.python.ops import rnn, rnn_cell
import numpy as np

from tensorflow.examples.tutorials.mnist import input_data

mnist = input_data.read_data_sets('MNIST_data', one_hot=True)

learning_rate = 3e-3
training_iters = 200000
batch_size = 100
displayStep = 100

n_input = 28  # we want the input to take the 28 pixels
n_steps = 28  # every 28
n_hidden = 400
n_classes = 10
cell_type = 'GRU'

sess = tf.InteractiveSession()

x = tf.placeholder('float', [None, n_steps, n_input])
y = tf.placeholder('float', [None, n_classes])

weights = {
    'out': tf.Variable(tf.random_normal([n_hidden, n_classes]))
}

biases = {
    'out': tf.Variable(tf.random_normal([n_classes]))
}


def RNN(x, weights, biases, cell_type='LSTM'):
    x = tf.transpose(x, [1,0,2])
    x = tf.reshape(x, [-1, n_input])
    x = tf.split(0, n_steps, x)  # Configuring so you can get it as needed for the 28 pixels

    n_hidden = weights['out'].get_shape().as_list()[0]
    if cell_type == 'RNN':
        cell = rnn_cell.BasicRNNCell(n_hidden)
    elif cell_type == 'GRU':
        cell = rnn_cell.GRUCell(n_hidden)
    else:
        cell = rnn_cell.BasicLSTMCell(n_hidden)

    outputs, states = rnn.rnn(cell, x, dtype=tf.float32)  # For the RNN where to get the output and hidden state

    return tf.matmul(outputs[-1], weights['out']) + biases['out']

pred = RNN(x, weights, biases, cell_type)

# Optimization: create the cost, optimization, evaluation, and accuracy
# For Cost: softmax_cross_entropy_with_logits works well
with tf.name_scope('train_loss'):
    cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(pred, y))
    tf.scalar_summary('train_cost', cost)

with tf.name_scope('test_loss'):
    cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(pred, y))
    tf.scalar_summary('test_cost', cost)

optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)

correct_pred = tf.equal(tf.argmax(pred, 1), tf.argmax(y, 1))
with tf.name_scope('train_stats'):
    with tf.name_scope('train_accuracy'):
        accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))
    tf.scalar_summary('train_accuracy', accuracy)

with tf.name_scope('test_stats'):
    with tf.name_scope('test_accuracy'):
        accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))
    tf.scalar_summary('test_accuracy', accuracy)

summary_op = tf.merge_all_summaries()
init = tf.initialize_all_variables()

# Instantiate a SummaryWriter to output summaries and the Graph.
optimizer_name = 'adam'
result_dir = 'results/' + cell_type + '/hidden-' + str(n_hidden) + '-alt/' + optimizer_name
result_dir += '/' + str(learning_rate)
train_writer = tf.train.SummaryWriter(result_dir + '/training', sess.graph)
test_writer = tf.train.SummaryWriter(result_dir + '/testing', sess.graph)

sess.run(init)

train_data = mnist.train.images.reshape((-1, n_steps, n_input))
train_label = mnist.train.labels
test_data = mnist.test.images.reshape((-1, n_steps, n_input))
test_label = mnist.test.labels

step = 0
while step * batch_size < training_iters:
    batch_x, batch_y = mnist.train.next_batch(batch_size)  # mnist has a way to get the next batch
    batch_x = batch_x.reshape((batch_size, n_steps, n_input))

    if step % displayStep == 0:
        train_acc = accuracy.eval(feed_dict={x: train_data, y: train_label})
        train_loss = cost.eval(feed_dict={x: train_data, y: train_label})
        train_summary_str = sess.run(summary_op, feed_dict={x: train_data, y: train_label})
        train_writer.add_summary(train_summary_str, step * batch_size)
        train_writer.flush()

        test_acc = accuracy.eval(feed_dict={x: test_data, y: test_label})
        test_loss = cost.eval(feed_dict={x: test_data, y: test_label})
        test_summary_str = sess.run(summary_op, feed_dict={x: test_data, y: test_label})
        test_writer.add_summary(test_summary_str, step * batch_size)
        test_writer.flush()

        print('Iter ' + str(step * batch_size) +
              ', Training Minibatch Loss=' + '{:.6f}'.format(train_loss) +
              ', Training Accuracy=' + '{:.5f}'.format(train_acc) +
              ', Testing Loss=' + '{:.6f}'.format(test_loss) +
              ', Testing Accuracy=' + '{:.5f}'.format(test_acc))

    step += 1
    sess.run(optimizer, feed_dict={x: batch_x, y: batch_y})

print('Optimization finished')
print('Testing Accuracy:', sess.run(accuracy, feed_dict={x: test_data, y: test_label}))
sess.close()

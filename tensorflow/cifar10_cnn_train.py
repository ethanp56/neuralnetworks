from scipy.misc import imread
# from scipy.ndimage import imread
import numpy as np
import tensorflow as tf
import random
# import matplotlib.pyplot as plt
# import matplotlib as mp
import time
import nn


def main():
    ntrain = 1000  # per class
    ntest = 100  # per class
    nclass = 10  # number of classes
    imsize = 28
    nchannels = 1
    batchsize = 100

    Train = np.zeros((ntrain * nclass, imsize, imsize, nchannels))
    Test = np.zeros((ntest * nclass, imsize, imsize, nchannels))
    LTrain = np.zeros((ntrain * nclass, nclass))
    LTest = np.zeros((ntest * nclass, nclass))

    print 'Preprocessing Data...'
    itrain = -1
    itest = -1
    for iclass in range(0, nclass):
        for isample in range(0, ntrain):
            path = './CIFAR10/Train/%d/Image%05d.png' % (iclass,isample)
            im = imread(path)  # 28 by 28
            im = im.astype(float) / 255
            itrain += 1
            Train[itrain, :, :, 0] = im
            LTrain[itrain, iclass] = 1  # 1-hot label
        for isample in range(0, ntest):
            path = './CIFAR10/Test/%d/Image%05d.png' % (iclass,isample)
            im = imread(path)  # 28 by 28
            im = im.astype(float) / 255
            itest += 1
            Test[itest, :, :, 0] = im
            LTest[itest, iclass] = 1  # 1-hot label

    sess = tf.InteractiveSession()

    x_image = tf.placeholder(tf.float32, [None, imsize, imsize, nchannels])  # tf variable for the data. Shape: [None, width, height, numberOfChannels]
    y_ = tf.placeholder(tf.float32, [None, nclass])  # tf variable for labels

    # --------------------------------------------------
    # model
    #create your model
    start_time = time.time()  # start timing
    print 'Building Computations...'

    act_name = 'relu'
    optimizer_name = 'adam'
    dropout_keep_prob = 0.04  # 1.0 -> No Dropout
    result_dir = 'results/' + act_name + '/' + optimizer_name
    keep_prob = tf.placeholder(tf.float32)
    y_conv = tf_cnn.init_cnn_with_summaries(x_image, keep_prob, act_name=act_name)

    # --------------------------------------------------
    # loss
    # Set up the loss, optimization, evaluation, and accuracy

    # Set up training with visualization
    with tf.name_scope('cross_entropy'):
        with tf.name_scope('total'):
            cross_entropy = tf.reduce_mean(-tf.reduce_sum(
                y_ * tf.log(y_conv), reduction_indices=[1]))
        tf.scalar_summary('cross-entropy', cross_entropy)

    with tf.name_scope('train'):
        if optimizer_name == 'adam':
            lr = 15.004e-4  # Current Opt: 3e-4
            train_step = tf.train.AdamOptimizer(lr).minimize(cross_entropy)
        elif optimizer_name == 'adadelta':
            lr = 3e-1
            train_step = tf.train.AdadeltaOptimizer(lr).minimize(cross_entropy)
        elif optimizer_name == 'sgd':
            lr = 2.02e-1  # Current Opt: 2e-1
            train_step = tf.train.GradientDescentOptimizer(lr).minimize(cross_entropy)
        elif optimizer_name == 'rmsprop':
            lr = 1e-3
            train_step = tf.train.RMSPropOptimizer(lr).minimize(cross_entropy)
        elif optimizer_name == 'momentum':
            lr = 3e-2
            momentum = 0.9
            train_step = tf.train.MomentumOptimizer(lr, momentum).minimize(cross_entropy)
    result_dir += '/' + str(lr)

    with tf.name_scope('train_accuracy'):
        with tf.name_scope('train_correct_prediction'):
            train_correct_prediction = tf.equal(
                tf.argmax(
                    y_conv, 1), tf.argmax(
                    y_, 1))
        with tf.name_scope('train_accuracy'):
            train_accuracy = tf.reduce_mean(tf.cast(train_correct_prediction, tf.float32))
        tf.scalar_summary('train_accuracy', train_accuracy)

    with tf.name_scope('test_accuracy'):
        with tf.name_scope('test_correct_prediction'):
            test_correct_prediction = tf.equal(
                tf.argmax(
                    y_conv, 1), tf.argmax(
                    y_, 1))
        with tf.name_scope('test_accuracy'):
            test_accuracy = tf.reduce_mean(tf.cast(test_correct_prediction, tf.float32))
        tf.scalar_summary('test_accuracy', test_accuracy)

    # Add a scalar summary for the snapshot loss.
    # Build the summary operation based on the TF collection of Summaries.
    # iterations (equivalently, after each epoch)
    summary_op = tf.merge_all_summaries()

    # Add the variable initializer Op.
    init = tf.initialize_all_variables()

    # Create a saver for writing training checkpoints.
    saver = tf.train.Saver()

    # Instantiate a SummaryWriter to output summaries and the Graph.
    train_writer = tf.train.SummaryWriter(result_dir + '/training', sess.graph)
    test_writer = tf.train.SummaryWriter(result_dir + '/testing', sess.graph)

    # --------------------------------------------------
    # Optimization

    print 'Results available in', result_dir
    print 'Training...'
    sess.run(init)
    batch_xs = np.zeros((batchsize, imsize, imsize, nchannels))
    batch_ys = np.zeros((batchsize, nclass))
    num_epochs = 40000
    for i in range(num_epochs):
        perm = np.arange(ntrain * nclass)
        np.random.shuffle(perm)
        for j in range(batchsize):
            batch_xs[j, :, :, :] = Train[perm[j], :, :, :]
            batch_ys[j, :] = LTrain[perm[j], :]

        if (i + 1) in [100, 200] or (i + 1) % 400 == 0:
            # Calculate train and test accuracy and print it
            print('Epoch %d, Train Acc: %g, Test Acc: %g' %
                  (i + 1,
                   train_accuracy.eval(feed_dict={x_image: Train, y_: LTrain, keep_prob: 1.0}),
                   test_accuracy.eval(feed_dict={x_image: Test, y_: LTest, keep_prob: 1.0})))

            # Tensorboard Training Logging
            summary_str = sess.run(summary_op, feed_dict={x_image: Train, y_: LTrain, keep_prob: dropout_keep_prob})
            train_writer.add_summary(summary_str, i)
            train_writer.flush()

            # Tensorboard Testing Logging
            summary_str = sess.run(summary_op, feed_dict={x_image: Test, y_: LTest, keep_prob: 1.0})
            test_writer.add_summary(summary_str, i)
            test_writer.flush()

        train_step.run(feed_dict={x_image: batch_xs, y_: batch_ys, keep_prob: dropout_keep_prob})  # Dropout for training only

    stop_time = time.time()
    print('The training takes %f second to finish' % (stop_time - start_time))
    sess.close()


if __name__ == '__main__':
    main()

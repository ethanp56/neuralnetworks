import random
import tensorflow as tf


def get_func(act_name):
    """
    Returns a tf activation function given an activation name
    """
    if act_name == 'relu':
        return tf.nn.relu
    elif act_name == 'tanh':
        return tf.nn.tanh
    elif act_name == 'elu':
        return tf.nn.elu
    elif act_name == 'softplus':
        return tf.nn.softplus
    elif act_name == 'softsign':
        return tf.nn.softsign
    elif act_name == 'sigmoid':
        return tf.nn.sigmoid
    else:
        print 'Bad Activation Function Name. Defaulting to ReLU'
        return tf.nn.relu


def weight_variable(shape, type='dense'):
    """
    Initialize weights
    :param shape: shape of weights, e.g. [w, h ,Cin, Cout] where
    w: width of the filters
    h: height of the filters
    Cin: the number of the channels of the filters
    Cout: the number of filters
    :return: a tensor variable for weights with initial values
    """
    if type == 'conv':
        return tf.get_variable('W' + str(random.random()), shape=shape, initializer=tf.contrib.layers.xavier_initializer_conv2d())
    return tf.get_variable('W' + str(random.random()), shape=shape, initializer=tf.contrib.layers.xavier_initializer())
    # return tf.Variable(tf.truncated_normal(shape, stddev=0.1))


def bias_variable(shape):
    """
    Initialize biases
    :param shape: shape of biases, e.g. [Cout] where
    Cout: the number of filters
    :return: a tensor variable for biases with initial values
    """
    # return tf.get_variable("b", shape=shape, initializer=tf.contrib.layers.xavier_initializer())
    return tf.Variable(tf.constant(0.1, shape=shape))


def variable_summaries(var, name):
    """
    Attach a lot of summaries to a Tensor.
    """
    with tf.name_scope('summaries'):
        mean = tf.reduce_mean(var)
        tf.scalar_summary('mean/' + name, mean)
        with tf.name_scope('stddev'):
            stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
        tf.scalar_summary('sttdev/' + name, stddev)
        tf.scalar_summary('max/' + name, tf.reduce_max(var))
        tf.scalar_summary('min/' + name, tf.reduce_min(var))
        tf.histogram_summary(name, var)


def dense_layer(input_tensor, W_dim, b_dim, layer_name, act=tf.nn.relu):
    """
    Reusable code for making a simple, fully-connected neural net layer.

    It does a matrix multiply, bias add, and then uses relu to nonlinearize.
    It also sets up name scoping so that the resultant graph is easy to read,
    and adds a number of summary ops.
    """
    # Adding a name scope ensures logical grouping of the layers in the graph.
    with tf.name_scope(layer_name):
        # This Variable will hold the state of the weights for the layer
        with tf.name_scope('net_inputs'):
            variable_summaries(input_tensor, layer_name + '/net_inputs')
        with tf.name_scope('weights'):
            weights = weight_variable(W_dim)
            variable_summaries(weights, layer_name + '/weights')
        with tf.name_scope('biases'):
            biases = bias_variable(b_dim)
            variable_summaries(biases, layer_name + '/biases')
        with tf.name_scope('activations'):
            preactivate = tf.matmul(input_tensor, weights) + biases
            activations = act(preactivate, 'activation')
            variable_summaries(activations, layer_name + '/activations')
        return activations


def conv_max_pool_layer(input_tensor, W_dim, b_dim,
                        layer_name, act=tf.nn.relu, visualize=False):
    """
    Reusable code for making a ConvNet layer.

    It does a convolution, bias add, and then uses relu to nonlinearize.
    It also sets up name scoping so that the resultant graph is easy to read,
    and adds a number of summary ops.
    """
    # Adding a name scope ensures logical grouping of the layers in the graph.
    with tf.name_scope(layer_name):
        # This Variable will hold the state of the weights for the layer
        with tf.name_scope('net_inputs'):
            variable_summaries(input_tensor, layer_name + '/net_inputs')
        with tf.name_scope('weights'):
            weights = weight_variable(W_dim, type='conv')
            variable_summaries(weights, layer_name + '/weights')

            # Weight Visualization: Credit - http://stackoverflow.com/questions/33783672/how-can-i-visualize-the-weightsvariables-in-cnn-in-tensorflow
            if visualize:
                # scale weights to [0 255] and convert to uint8 (maybe change scaling?)
                x_min = tf.reduce_min(weights)
                x_max = tf.reduce_max(weights)
                weights_0_to_1 = (weights - x_min) / (x_max - x_min)
                weights_0_to_255_uint8 = tf.image.convert_image_dtype(weights_0_to_1, dtype=tf.uint8)

                # to tf.image_summary format [batch_size, height, width, channels]
                weights_transposed = tf.transpose(weights_0_to_255_uint8, [3, 0, 1, 2])

                # this will display random 3 filters from the 64 in conv1
                tf.image_summary('Conv1/filters', weights_transposed, max_images=32)

        with tf.name_scope('biases'):
            biases = bias_variable(b_dim)
            variable_summaries(biases, layer_name + '/biases')
        with tf.name_scope('activations'):
            preactivate = tf.nn.conv2d(
                input_tensor, weights, strides=[
                    1, 1, 1, 1], padding='SAME') + biases
            activations = act(preactivate, 'activation')
            variable_summaries(activations, layer_name + '/activations')
        with tf.name_scope('activations_pooled'):
            pooled = tf.nn.max_pool(
                activations, ksize=[
                    1, 2, 2, 1], strides=[
                    1, 2, 2, 1], padding='SAME')
            variable_summaries(pooled, layer_name + '/activations_pooled')
        return pooled


def init_rec_dense_with_summaries(x_image, h_sizes, act_name='relu'):
    """
    Initializes the Architecture and Parameters of a Dense NN for Reconstruction
    :param x_image: input data, reshaped into proper 4-D input to a ConvNet
    :param hidden_layer_sizes: sizes of the hidden dense layers
    :return: the NN tensor that gives output predictions
    """
    act_func = get_func(act_name)
    orig_dim = x_image.get_shape()[1]
    if isinstance(h_sizes, int):
        temp = []
        for i in range(5):
            temp.append(num_hidden)
        h_sizes = temp
    hidden1 = dense_layer(x_image, [orig_dim, h_sizes[0]], [h_sizes[0]], 'Dense1', act_func)
    hidden2 = dense_layer(hidden1, [h_sizes[0], h_sizes[1]], [h_sizes[1]], 'Dense2', act_func)
    hidden3 = dense_layer(hidden2, [h_sizes[1], h_sizes[2]], [h_sizes[2]], 'Dense3', act_func)
    hidden4 = dense_layer(hidden3, [h_sizes[2], h_sizes[3]], [h_sizes[3]], 'Dense4', act_func)
    hidden5 = dense_layer(hidden4, [h_sizes[3], h_sizes[4]], [h_sizes[4]], 'Dense5', act_func)
    y_rec = dense_layer(hidden5, [h_sizes[4], orig_dim], [orig_dim], 'Reconstruction', tf.nn.elu)  # tf.identity for linear
    return y_rec


def init_cnn_with_summaries(x_image, keep_prob, act_name='relu'):
    """
    Initializes the Architecture and Parameters of a ConvNet
    :param x_image: input data, reshaped into proper 4-D input to a ConvNet
    :param keep_prob: the probability that a neuron is used (for dropout training)
    :return: the ConvNet tensor that gives output predictions
    """
    act_func = get_func(act_name)

    h_conv_pool1 = conv_max_pool_layer(x_image, [5, 5, 1, 32], [32], 'Conv1', act_func, visualize=True)
    h_conv_pool2 = conv_max_pool_layer(
        h_conv_pool1, [5, 5, 32, 64], [64], 'Conv2', act_func)
    # TODO: Why is image 7x7 here? With Same Padding prev?
    h_conv_pool2_flat = tf.reshape(h_conv_pool2, [-1, 7 * 7 * 64])
    h_fc1 = dense_layer(
        h_conv_pool2_flat, [
            7 * 7 * 64, 1024], [1024], 'Dense1', act_func)
    # h_fc1_drop = dropout_layer(h_fc1, 'Dropout', keep_prob)
    h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)
    y_conv = dense_layer(h_fc1_drop, [1024, 10], [
                         10], 'Softmax', tf.nn.softmax)
    return y_conv

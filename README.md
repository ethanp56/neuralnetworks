# NeuralNetworks
Building and Experimenting with Neural Networks.

This repo contains:
1) My own Python + Numpy Neural Network implementation,
2) A TensorFlow implemented Convolutional Neural Network,
in addition to code to employ these neural network implementations on data sets,
as well as visualize the training, network, and results
(i.e. train, validation, & test accuracies, weights, etc.)
3) A Tensorflow implemented Recurrent Neural Network
4) Theano Neural Network implementations

Used for personal, quick low-level and high-level Neural Networking experimentation.
Some Tensorflow code based off Google tutorials.
Some Theano code based off Alec Radford's Theano Tutorials Repo:
- https://github.com/Newmu/Theano-Tutorials

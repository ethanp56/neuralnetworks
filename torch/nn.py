
from __future__ import print_function
import torch
import torchvision
import torchvision.datasets
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.init import xavier_normal, xavier_uniform, calculate_gain
from torch.autograd import Variable
from tqdm import tqdm

# Read in data
normalize = torchvision.transforms.Compose([
        torchvision.transforms.ToTensor(),
        torchvision.transforms.Normalize(
            mean=[0.5, 0.5, 0.5],
            std=[0.5, 0.5, 0.5],
        )
    ])
train = torchvision.datasets.MNIST('data/MNIST/', train=True, transform=normalize, target_transform=torchvision.transforms.ToTensor(), download=True)
test = torchvision.datasets.MNIST('data/MNIST/', train=False, transform=normalize, target_transform=torchvision.transforms.ToTensor(), download=True)

X_train = Variable(train.train_data.view(-1, 28 * 28).float())
X_test = Variable(test.test_data.view(-1, 28 * 28).float())
y_train = Variable(train.train_labels.long())
y_test = Variable(test.test_labels.long())
num_train = y_train.size()[0]
num_test = y_test.size()[0]

# Initialize model weights
def init_weights(shape, nonlin='relu'):
    W = torch.Tensor(*shape)
    xavier_normal(W, gain=calculate_gain(nonlin))
    return Variable(W, requires_grad=True)

w1 = init_weights((28 * 28, 14 * 14))
w2 = init_weights((14 * 14, 7 * 7))
w3 = init_weights((7 * 7, 10))

# Train model
alpha = 0.001
num_epochs = 50
for i in range(num_epochs):
    l1 = F.relu(X_train.mm(w1))
    l2 = F.relu(l1.mm(w2))
    l3 = l2.mm(w3)
    loss = F.cross_entropy(l3, y_train)
    loss.backward()
    w1.add_(-alpha * w1.grad)
    w2.add_(-alpha * w2.grad)
    w3.add_(-alpha * w3.grad)
    w1.grad.data.zero_()
    w2.grad.data.zero_()
    w3.grad.data.zero_()
    if i % 5 == 0:
        print(loss)
print('Done Training!')

# Test model
_, y_pred = torch.max(log_probs, 1)
correct = (y_pred == y_train).squeeze()
print('Train Accuracy:', correct.data.sum() / float(num_train))
